package com.xapos259;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos259Application {

	public static void main(String[] args) {
		SpringApplication.run(Xapos259Application.class, args);
	}

}
